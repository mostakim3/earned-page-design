import 'package:design_page_3/Widgets/earned_widget.dart';
import 'package:flutter/material.dart';

class TabBarScreen extends StatefulWidget {
  const TabBarScreen({super.key});

  @override
  State<TabBarScreen> createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
              indicatorWeight: 3,
              indicatorPadding: EdgeInsets.symmetric(horizontal: 20),
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: Colors.orange,
              tabs: [
                Tab(
                  child: Text(
                    "Earned",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                Tab(
                  child: Text(
                    "Spent",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                )
              ]),
          leading: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              )),
          backgroundColor: const Color.fromARGB(255, 11, 97, 167),
          title: const Text(
            "Point History",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body:  TabBarView(
          children: [
            ListView.builder(
              itemCount: 10,
              itemBuilder: (context,index){
              return  EarnedListTileWidget(
                  title: "Comment on post",
                  subTitle: "8:33 PM 06-12-2023",
                  points: "+1 points",
                );
            }),
            ListView.builder(
                itemCount: 10,
                itemBuilder: (context,index){
                return  EarnedListTileWidget(
                    title: "Comment on post",
                    subTitle: "8:33 PM 06-12-2023",
                    points: "+1 points",
                  );
              }),
            
            
          ],
        ),
      ),
    );
  }
}
