import 'package:flutter/material.dart';

class EarnedListTileWidget extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? points;
  const EarnedListTileWidget({super.key, this.points, this.subTitle, this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          height: 75,
          width: double.infinity,
          child: ListTile(
            leading: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title!,
                  style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      color: Colors.black),
                ),
                Text(
                  subTitle!,
                  style: const TextStyle(fontSize: 13, color: Colors.black45),
                )
              ],
            ),
            trailing: Text(
              points!,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: Color.fromARGB(255, 11, 97, 167),
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border.all(width: 0.1, color: Colors.black)),
        )
      ],
    );
  }
}
